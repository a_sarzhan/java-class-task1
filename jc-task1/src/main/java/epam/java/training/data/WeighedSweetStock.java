package epam.java.training.data;


public enum WeighedSweetStock {
    TOPLENOE_MOLOKO("Toplenoe moloko", SweetType.BISCUIT, 550, 0.23),
    ZOLOTAYA_LILIYA("Zolotaya Liliya", SweetType.CANDY, 2300, 0.56),
    KARAKUM("Karakum", SweetType.CHOCOLATE, 2450, 0.45),
    MARSIANKA("Marsianka", SweetType.CANDY, 2300, 0.52),
    ZMEIKI("Zmeiki", SweetType.FRUIT_JELLY, 1200, 0.44),
    TOFFIX("Toffix", SweetType.CHEWY_CANDY, 2000, 0.44),
    ALATAU("Alatau", SweetType.BISCUIT, 560, 0.18),
    MASKA("Maska", SweetType.CHOCOLATE, 1890, 0.52),
    BARBARIS("Barbaris", SweetType.CHEWY_CANDY, 1300, 0.56),
    KOROVKA("Korovka", SweetType.CANDY, 1600, 0.35),
    PTICHE_MOLOKO("Ptiche Moloko", SweetType.CANDY, 1890, 0.55);

    private String name;
    private double cost;
    private double sugarRatio;
    private SweetType type;

    WeighedSweetStock(String name, SweetType type, double cost, double sugarRatio){
        this.name = name;
        this.type = type;
        this.cost = cost;
        this.sugarRatio = sugarRatio;
    }

    public String getName() {
        return name;
    }

    public double getCost() {
        return cost;
    }

    public double getSugarRatio() {
        return sugarRatio;
    }

    public SweetType getType() {
        return type;
    }

    @Override
    public String toString() {
        return String.format("%-20s | %-15s | %15.2f %s", name, type, cost, "(Tg)");
    }

}
