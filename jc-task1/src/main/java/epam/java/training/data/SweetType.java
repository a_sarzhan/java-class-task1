package epam.java.training.data;

public enum SweetType {
    CHOCOLATE, CANDY, CHEWY_CANDY, LOLLIPOP, BISCUIT, WAFFLE, CUPCAKE, FRUIT_JELLY;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }

}
