package epam.java.training.data;


public enum PackedSweetStock {
    SNICKERS("Snickers", SweetType.CHOCOLATE, 0.08, 250, 0.43),
    MARS("Mars", SweetType.CHOCOLATE, 0.07, 230, 0.49),
    TWIX("Twix", SweetType.CHOCOLATE, 0.08, 250, 0.45),
    BELOSNEZHKA("Belosnezhka", SweetType.WAFFLE, 0.25, 450, 0.37),
    FRUITELLA("Fruitella", SweetType.FRUIT_JELLY, 0.20, 330, 0.39),
    MAMBA("Mamba", SweetType.CHEWY_CANDY, 0.15, 550, 0.55),
    KINDER_SURPRISE("Kinder Surprise", SweetType.CHOCOLATE, 0.2, 450, 0.56),
    BOUNTY("Bounty", SweetType.CHOCOLATE, 0.07, 180, 0.50),
    KIT_KAT("KitKat", SweetType.CHOCOLATE, 0.07, 180, 0.51),
    ZEFIR("Zefir", SweetType.CUPCAKE, 0.35, 450, 0.56),
    ROSHEN_WAFERS("Roshen Wafers", SweetType.WAFFLE, 0.30, 330, 0.45),
    BONDI("Bondi", SweetType.BISCUIT, 0.30, 550, 0.21),
    BARNI("Barni", SweetType.BISCUIT, 0.35, 450,0.34),
    TICK_TACK("Tick-Tack", SweetType.CANDY, 0.05, 120, 0.54),
    SKITTLES("Skittles", SweetType.CHEWY_CANDY, 0.10, 460, 0.53),
    ZOO_BOOM("Zoo Boom", SweetType.BISCUIT, 0.25, 345, 0.23),
    MANDMS("M&Ms", SweetType.CANDY, 0.15, 460, 0.53),
    CHUPACHUPS("Chupa Chups", SweetType.LOLLIPOP, 0.05, 80, 0.56);

    private String name;
    private double weight;
    private double cost;
    private double sugarRatio;
    private SweetType type;

    PackedSweetStock(String name, SweetType type, double weight, double cost, double sugarRatio){
        this.name = name;
        this.type = type;
        this.weight = weight;
        this.cost = cost;
        this.sugarRatio = sugarRatio;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public double getCost() {
        return cost;
    }

    public double getSugarRatio() {
        return sugarRatio;
    }

    public SweetType getType() {
        return type;
    }

    @Override
    public String toString() {
        return String.format("%-20s | %-15s | %15.2f %s", name, type, cost, "(Tg)");
    }

}
