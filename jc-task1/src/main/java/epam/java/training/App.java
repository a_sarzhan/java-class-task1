package epam.java.training;


import epam.java.training.actions.PresentAction;
import epam.java.training.actions.UserConsoleInteraction;
import epam.java.training.entities.Present;

import java.io.IOException;

/**
 * Epam - Java Web Development Training
 * Java Class - Task1
 * Assel Sarzhanova
 */
public class App {

    public static void main( String[] args ) throws IOException {
        UserConsoleInteraction.getConsoleInstance().printWelcomeScreen();

        PresentAction presentAction = PresentAction.getPresentInstance();
        Present present = presentAction.createPresent();

        presentAction.sortPresentByUserOption(present);
        presentAction.printPresentContent(present);
        presentAction.printPresentTotalWeightAndCost(present);
        presentAction.printSweetInSugarLevelRange(present);

    }

}
