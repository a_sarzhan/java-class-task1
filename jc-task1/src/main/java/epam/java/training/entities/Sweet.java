package epam.java.training.entities;

import epam.java.training.data.SweetType;

import java.util.Objects;

public class Sweet implements Comparable{
    private String name;
    private double weight;
    private double cost;
    private double sugarRatio;
    private SweetType type;

    public Sweet(String name, double weight, double costPerKilogram, double sugarRatio, SweetType type){
        this.name = name;
        this.weight = weight;
        this.cost = costPerKilogram;
        this.sugarRatio = sugarRatio;
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setSugarRatio(double sugarRatio) {
        this.sugarRatio = sugarRatio;
    }

    public void setType(SweetType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public double getSugarRatio() {
        return sugarRatio;
    }

    public double getCost() {
        return cost;
    }

    public SweetType getType() {
        return type;
    }

    public double getCostAfterCalculation() {
        return this.weight * this.cost;
    }

    public double getSugarLevelInKilogram(){
        return this.weight * this.sugarRatio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sweet)) return false;
        Sweet sweet = (Sweet) o;
        return getName().equals(sweet.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    @Override
    public String toString() {
        String format = "%-18s | %-12s | %-10.2f | %-10.2f | %-15.2f | %-8s";
        return String.format(format, name, type, weight, getCostAfterCalculation(), getSugarLevelInKilogram(), "");
    }

    @Override
    public int compareTo(Object o) {
        return this.name.compareTo(((Sweet) o).name);
    }

}
