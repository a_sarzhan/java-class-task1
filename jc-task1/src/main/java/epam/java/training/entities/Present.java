package epam.java.training.entities;

import java.util.*;

public class Present {
    private double packageWeight;
    private double packageCost;
    private double presentCost;
    private double presentWeight;
    private List<Sweet> sweetList;

    public Present(double packageWeight, double packageCost, List<Sweet> sweetList){
        this.packageWeight = packageWeight;
        this.packageCost = packageCost;
        this.sweetList = sweetList;
    }

    public List<Sweet> getSweetList() {
        return sweetList;
    }

    public double getPackageWeight() {
        return packageWeight;
    }

    public double getPackageCost() {
        return packageCost;
    }

    public void setPresentCost(double presentCost) {
        this.presentCost = presentCost;
    }

    public void setPresentWeight(double presentWeight) {
        this.presentWeight = presentWeight;
    }

    public double getPresentCost() {
        return presentCost;
    }

    public double getPresentWeight() {
        return presentWeight;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        String format = "%-3s | %-18s | %-12s | %-10s | %-10s | %-15s | %-8s";
        stringBuilder.append(String.format(format, "#", "Name", "Type", "Weight(Kg)", "Cost(Tg)", "Sugar Level(Kg)", "Amount"));
        stringBuilder.append("\n");
        stringBuilder.append("-----------------------------------------------------------------------------------------");
        stringBuilder.append("\n");
        for (int i = 0; i < sweetList.size(); i++) {
            stringBuilder.append(String.format("%-3d | ", i + 1) + sweetList.get(i));
            stringBuilder.append("\n");
        }
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

}
