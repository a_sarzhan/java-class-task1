package epam.java.training.entities;

import epam.java.training.data.SweetType;

public class PackedSweet extends Sweet {
    private int amount;

    public PackedSweet(String name, double weight, double costPerUnit, int amount, double sugarRatio, SweetType type){
        super(name, weight, costPerUnit, sugarRatio, type);
        this.amount = amount;
    }

    @Override
    public double getCostAfterCalculation() {
        return getCost() * amount;
    }

    @Override
    public String toString() {
        String format = "%-18s | %-12s | %-10.2f | %-10.2f | %-15.2f | %-8d";
        return String.format(format, getName(), getType(), getWeight(), getCostAfterCalculation(), getSugarLevelInKilogram(), amount);
    }

}
