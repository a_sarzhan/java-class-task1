package epam.java.training.actions;

import epam.java.training.entities.PackedSweet;
import epam.java.training.entities.Sweet;
import epam.java.training.data.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class UserConsoleInteraction {
    private static UserConsoleInteraction consoleInstance;
    private final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    private double packageCost;
    private double packageWeight;
    private final List<String> packageStock = new ArrayList<>();
    private final List<String> sortOptionList = new ArrayList<>();

    {
        packageStock.add("Bag     | 0.05 | 250");
        packageStock.add("Box     | 0.15 | 450");
        packageStock.add("Wrapper | 0.10 | 150");

        sortOptionList.add("Sort by Sweet weight");
        sortOptionList.add("Sort by Sweet cost");
        sortOptionList.add("Sort by Sweet sugar level");
    }

    private UserConsoleInteraction() {}

    public static UserConsoleInteraction getConsoleInstance(){
        if (consoleInstance == null) {
            consoleInstance = new UserConsoleInteraction();
        }
        return consoleInstance;
    }

    public void printWelcomeScreen() {
        System.out.println("*********************************************************");
        System.out.println("Let's make the best \"New Year Present\" for children!!!");
        System.out.println("*********************************************************");
        System.out.println();
    }

    Set<Sweet> getWeighedSweetsSet() throws IOException {
        Set<Sweet> weighedSweetSet = new HashSet<>();
        int usersOrderNumber;
        double usersSweetWeight;
        String userInput;
        WeighedSweetStock usersSweet;

        System.out.println("Sweets by weight:");
        System.out.println("--------------------------------------\n");

        for (int i = 0; i < WeighedSweetStock.values().length; i++) {
            System.out.println(String.format("%-2d | %s", i + 1, WeighedSweetStock.values()[i]));
        }

        System.out.println();

        while (true) {
            try {
                System.out.println("Enter sweets Order number, weight(split by space). Ex.: [3 0.30]");
                System.out.println("Enter \"exit\" to finish");
                userInput = bufferedReader.readLine();

                if (userInput.trim().equalsIgnoreCase("exit")) {
                    break;
                }

                String[] userInputArray = userInput.split("\\s+");
                usersOrderNumber = Integer.parseInt(userInputArray[0].trim());
                usersSweetWeight = Double.parseDouble(userInputArray[1].trim());

                if (usersOrderNumber <= 0 || usersOrderNumber > WeighedSweetStock.values().length) {
                    System.out.println("Invalid Order number. Try again");
                    continue;
                }

                if (usersSweetWeight <= 0) {
                    System.out.println("Invalid Weight.Try again");
                    continue;
                }

                usersSweet = WeighedSweetStock.values()[usersOrderNumber - 1];
                weighedSweetSet.add(new Sweet(usersSweet.getName(), usersSweetWeight, usersSweet.getCost(), usersSweet.getSugarRatio(), usersSweet.getType()));
            } catch (NumberFormatException e) {
                System.out.println("Invalid Input. Try again");
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Invalid input. Try again");
            }
        }

        return weighedSweetSet;
    }

    Set<Sweet> getPackedSweetsSet() throws IOException {
        Set<Sweet> packedSweetSet = new HashSet<>();
        int usersOrderNumber;
        int usersSweetAmount;
        String userInput;
        PackedSweetStock usersSweet;

        System.out.println("Packed sweets:");
        System.out.println("--------------------------------------\n");

        for (int i = 0; i < PackedSweetStock.values().length; i++) {
            System.out.println(String.format("%-2d | %s", i + 1, PackedSweetStock.values()[i]));
        }

        System.out.println();

        while (true) {
            try {
                System.out.println("Enter sweets Order number, amount(split by space). Ex.: [3 4]");
                System.out.println("Enter \"exit\" to finish");
                userInput = bufferedReader.readLine();

                if (userInput.trim().equalsIgnoreCase("exit")) {
                    break;
                }

                String[] userInputArray = userInput.split("\\s+");
                usersOrderNumber = Integer.parseInt(userInputArray[0].trim());
                usersSweetAmount = Integer.parseInt(userInputArray[1].trim());

                if (usersOrderNumber <= 0 || usersOrderNumber > PackedSweetStock.values().length) {
                    System.out.println("Invalid Order number. Try again");
                    continue;
                }
                if (usersSweetAmount <= 0) {
                    System.out.println("Invalid Amount. Try again");
                    continue;
                }
                usersSweet = PackedSweetStock.values()[usersOrderNumber - 1];
                packedSweetSet.add(new PackedSweet(usersSweet.getName(), usersSweet.getWeight(), usersSweet.getCost(), usersSweetAmount, usersSweet.getSugarRatio(), usersSweet.getType()));
            } catch (NumberFormatException e) {
                System.out.println("Invalid Input. Try again");
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Invalid input. Try again");
            }
        }

        return packedSweetSet;
    }

    void setPresentWrapCostAndWeight() throws IOException {
        String userInputString;
        int userInputNumber;
        System.out.println("Wrapping options:");
        System.out.println("------------------------------");

        for (int i = 0; i < packageStock.size(); i++) {
            System.out.println((i + 1 )+ " | " + packageStock.get(i));
        }

        System.out.println();

        while (true) {
            try {
                System.out.println("Enter wrapping order number: ");
                userInputString = bufferedReader.readLine();
                userInputNumber = Integer.parseInt(userInputString);

                if (userInputNumber <=0 || userInputNumber > packageStock.size()) {
                    System.out.println("Invalid order. Try again");
                    continue;
                }

                packageWeight = Double.parseDouble(packageStock.get(userInputNumber - 1).split("\\|")[1].trim());
                packageCost = Double.parseDouble(packageStock.get(userInputNumber - 1).split("\\|")[2].trim());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Try again");
            }
        }

    }

    double getPackageCost() {
        return packageCost;
    }

    double getPackageWeight() {
        return packageWeight;
    }

    int getPresentSortOption() throws IOException {
        int optionNumber;
        System.out.println("Present sorting options:");
        System.out.println("------------------------------");

        for (int i = 0; i < sortOptionList.size(); i++) {
            System.out.println((i + 1) + " | " + sortOptionList.get(i));
        }

        System.out.println();

        while (true) {
            try {
                System.out.println("Enter Option Order Number:");
                optionNumber = Integer.parseInt(bufferedReader.readLine());

                if (optionNumber <= 0 || optionNumber > sortOptionList.size()) {
                    System.out.println("Invalid Option Order. Try again");
                    continue;
                }
                break;
            } catch (NumberFormatException e) {
                System.out.println("Invalid Input. Try again");
            }
        }

        return optionNumber;
    }

    double getUsersSugarLevelBound() throws IOException {
        double bound;

        while (true) {
            try {
                System.out.println("Enter Sugar level bound:");
                bound = Double.parseDouble(bufferedReader.readLine());
                if (bound < 0) {
                    System.out.println("Negative input.Try again");
                    continue;
                }
                break;
            } catch (NumberFormatException e) {
                System.out.println("Invalid Input. Try again");
            }
        }

        return bound;
    }

}
