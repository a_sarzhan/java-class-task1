package epam.java.training.actions;

import epam.java.training.entities.Present;
import epam.java.training.entities.Sweet;

import java.io.IOException;
import java.util.*;

public class PresentAction {
    private static PresentAction presentInstance;
    private UserConsoleInteraction consoleReader = UserConsoleInteraction.getConsoleInstance();

    private PresentAction() {}

    public static PresentAction getPresentInstance() {
        if (presentInstance == null) {
            presentInstance = new PresentAction();
        }
        return presentInstance;
    }

    public Present createPresent() throws IOException {
        Set<Sweet> allSweetSet = new HashSet<>();
        Set<Sweet> weighedSweetSet = consoleReader.getWeighedSweetsSet();
        Set<Sweet> packedSweetSet = consoleReader.getPackedSweetsSet();

        if (!weighedSweetSet.isEmpty()) {
            allSweetSet.addAll(weighedSweetSet);
        }

        if (!packedSweetSet.isEmpty()) {
            allSweetSet.addAll(packedSweetSet);
        }

        if (allSweetSet.isEmpty()) {
            System.out.println("-----------------------------------------------");
            System.out.println("Your present is empty. Please put some sweets!");
            System.out.println("-----------------------------------------------");

            consoleReader.getWeighedSweetsSet();
            consoleReader.getPackedSweetsSet();
        }

        consoleReader.setPresentWrapCostAndWeight();
        double wrapWeight = consoleReader.getPackageWeight();
        double wrapCost = consoleReader.getPackageCost();

        return new Present(wrapWeight, wrapCost, new ArrayList<Sweet>(allSweetSet));
    }

    public void sortPresentByUserOption(Present present) throws IOException {
        int sortOptionNumber = consoleReader.getPresentSortOption();

        switch (sortOptionNumber) {
            case 1:
                sortPresentBySweetWeight(present.getSweetList());
                break;
            case 2:
                sortPresentBySweetCost((present.getSweetList()));
                break;
            case 3:
                sortPresentBySugarLevel(present.getSweetList());
                break;
            default:
                sortPresentBySweetName(present.getSweetList());
                break;
        }

    }

    private void sortPresentBySweetWeight(List<Sweet> sweetList) {
        sweetList.sort(new Comparator<Sweet>() {
            @Override
            public int compare(Sweet o1, Sweet o2) {
                return Double.compare(o1.getWeight(), o2.getWeight());
            }
        });
    }

    private void sortPresentBySweetCost(List<Sweet> sweetList) {
        sweetList.sort(Comparator.comparing(Sweet::getCostAfterCalculation));
    }


    private void sortPresentBySugarLevel(List<Sweet> sweetList) {
        sweetList.sort(Comparator.comparing(Sweet::getSugarLevelInKilogram));
    }

    private void sortPresentBySweetName(List<Sweet> sweetList){
        Collections.sort(sweetList);
    }

    private void calculateAndSetPresentCost(Present present) {
        double totalCost = 0;

        for (Sweet sweet : present.getSweetList()) {
            totalCost += sweet.getCostAfterCalculation();
        }

        totalCost += present.getPackageCost();
        present.setPresentCost(totalCost);
    }

    private void calculateAndSetPresentWeight(Present present) {
        double totalWeight = 0;
        for (Sweet sweet : present.getSweetList()) {
            totalWeight += sweet.getWeight();
        }

        totalWeight += present.getPackageWeight();
        present.setPresentWeight(totalWeight);
    }

    private Optional<Sweet> findBySugarLevelRange(Present present, double lowerBound, double upperBound) {
        return present.getSweetList().stream().filter(o ->
                o.getSugarLevelInKilogram() >= lowerBound && o.getSugarLevelInKilogram() <= upperBound).findFirst();
    }

    public void printPresentTotalWeightAndCost(Present present) {
        calculateAndSetPresentCost(present);
        calculateAndSetPresentWeight(present);
        System.out.println(String.format("Present Cost = %.2f %s, Present Weight = %.2f %s", present.getPresentCost(), "(Tg)", present.getPresentWeight(), "(Kg)"));
    }

    public void printPresentContent(Present present) {
        System.out.println(present);
    }

    public void printSweetInSugarLevelRange(Present present) throws IOException {
        double lowerBound = consoleReader.getUsersSugarLevelBound();
        double temporaryBound = consoleReader.getUsersSugarLevelBound();

        double upperBound = (lowerBound > temporaryBound) ? lowerBound : temporaryBound;
        lowerBound = (upperBound == lowerBound) ? temporaryBound : lowerBound;

        Optional<Sweet> sweetOptional = findBySugarLevelRange(present, lowerBound, upperBound);

        if (sweetOptional.isPresent()) {
            System.out.println(sweetOptional.get());
        } else {
            System.out.println("No Sweet found within the sugar level range (" + lowerBound + ", " + upperBound + ").");
        }
    }

}
